# Практики по майнору в ВШЭ
Здесь вы найдете оглавление по практикам за разные года и по разным курсам.

## Весна 2024 Введение в Python (2 курс)
Курс посвящен введению в Python, [задания](https://stepik.org/56391) есть на Stepik.

Код с практик лежит [здесь](https://gitlab.com/sigmadt/hse-minor/-/tree/intro-python-spring-2024?ref_type=heads).


## Осень 2024 Введение в алгоритмы (3 курс)
Курс посвящен введению в Python, [задания](https://stepik.org/63216) есть на Stepik.

Код с практик лежит [здесь](https://gitlab.com/sigmadt/hse-minor/-/tree/intro-algo-fall-2024?ref_type=heads).

